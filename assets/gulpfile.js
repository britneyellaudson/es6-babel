var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var jade = require('gulp-jade');
var sass = require("gulp-sass");
var sassGlob = require('gulp-sass-glob');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var autoprefixer = require('gulp-autoprefixer');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var scsslint = require('gulp-scss-lint');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var critical = require('critical').stream;
var babel = require('gulp-babel');
var through = require('through2');

function logError(err) {
    if (err.file) {
        console.log("File ==>", err.file);
    }

    if (err.fileName) {
        console.log("File ==>", err.fileName);
    }

    if (err.line) {
        console.log("Line ==>", err.line);
    }

    if (err.lineNumber) {
        console.log("Line ==>", err.lineNumber);
    }

    if (err.message) {
        console.log("Full message ==>", err.message);
    }
}

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: ['html', './']
        }
    });
});

gulp.task('browserSync-resume', function () {
    browserSync.resume();
});

gulp.task('jade', function () {
    gulp.src('jade/*.jade')
        .pipe(jade({
            pretty: true,
        }))
        .on('error', function (err) {
            browserSync.pause();
            browserSync.notify('<div style="color:red">Jade fail!!!</div>', 30000);
            console.log('++++++++++++++++ Jade fails ++++++++++++++++');
            logError(err);
        })
        .pipe(gulp.dest('html/'))
        .pipe(browserSync.stream());
});

gulp.task('scss-lint', function () {
    return gulp.src([
        'scss/blocks/**/*.scss',
        'scss/components/**/*.scss',
        'scss/fonts/**/*.scss',
        'scss/helpers/**/*.scss',
        'scss/pages/**/*.scss',
        'scss/print/**/*.scss',
        'scss/reset/**/*.scss',
        'scss/responsive/**/*.scss',
        'scss/*.scss'
    ]).pipe(scsslint({
        'config': 'SCSS-lint.yml',
    }));
});

gulp.task('scss', function () {
    return gulp.src('scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', function (err) {
            browserSync.pause();
            console.log('++++++++++++++++ SCSS fails ++++++++++++++++');
            logError(err);
            browserSync.notify('<div style="color:red">SCSS fail!!!</div>', 30000);
            this.emit('end');
        }))
        .pipe(autoprefixer({
            browsers: ['IE >= 9', 'Firefox > 20', 'iOS 7']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css/'));
});

gulp.task('scss-deploy', ['scss'], function () {
    return gulp.src(['css/*.css', '!css/*.min.css'])
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('css/'))
        .pipe(browserSync.stream());
});

gulp.task('jshint', function () {
    return gulp.src('js/main.dev.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('js-libs', function() {
    return gulp.src([
            '!js/libs/jQuery.js',
            'js/libs/*.js',
            'js/plugins/*.js'
        ])
        .pipe(plumber())
        .on('error', function (err) {
            browserSync.pause();
            console.log('++++++++++++++++ JS fails ++++++++++++++++');
            logError(err);
            browserSync.notify('<div style="color:red">JS fail!!!</div>', 30000);
        })
        .pipe(uglify())
        .pipe(concat('libs.js'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('js/'))
});

gulp.task('js-libs-concat', ['js-libs'], function() {
    return gulp.src([
            'js/libs.js',
            'js/main.js'
        ])
        .pipe(plumber())
        .pipe(concat('main.min.js'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('js/'))
        .pipe(browserSync.stream());
});

// gulp.task('js-main', function () {
//     return gulp.src('js/main.dev.js')
//         .pipe(plumber())
//         .on('error', function (err) {
//             browserSync.pause();
//             console.log('++++++++++++++++ JS fails ++++++++++++++++');
//             logError(err);
//             browserSync.notify('<div style="color:red">JS fail!!!</div>', 30000);
//         })
//         .pipe(uglify())
//         .pipe(concat('main.js'))
//         .pipe(plumber.stop())
//         .pipe(gulp.dest('js/'))
//         .pipe(browserSync.stream());
// });

gulp.task('js-main-concat', ['babel'], function() {
    return gulp.src([
            'js/libs.js',
            'js/main.js'
        ])
        .pipe(plumber())
        .pipe(concat('main.min.js'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('js/'))
        .pipe(browserSync.stream());
});

gulp.task('critical', function () {
    return gulp.src('html/*.html')
        .pipe(critical({
            base: 'html/',
            inline: false,
            css: ['css/screen.css'],
            minify: false,
            ignore: ['@font-face']
        }))
        .pipe(gulp.dest('html/'));
});

gulp.task('babel', function() {
    return gulp.src('js/main.dev.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('main.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('js/'));
});

gulp.task('watch', function () {
    gulp.watch('jade/**/*.jade', ['jade', 'browserSync-resume']);
    gulp.watch('scss/**/*.scss', ['scss-deploy', 'browserSync-resume']);
    gulp.watch(['js/libs/*.js', 'js/plugins/*.js'], ['js-libs-concat', 'browserSync-resume']);
    gulp.watch(['js/main.dev.js'], ['js-main-concat', 'browserSync-resume']);
});

gulp.task('default', ['jade', 'scss-deploy', 'js-libs', 'js-main-concat', 'watch', 'browser-sync'], function () {
    console.log('/***DONE***/');
});

gulp.task('deploy', ['scss-deploy', 'js-main-deploy'], function () {
    gulp.start('critical');
    console.log('/***CSS, JS minfile and critical CSS was created!!!***/');
});