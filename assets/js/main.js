'use strict';

console.log('-------------Array---------------');

var arr1 = ['a', 'b', 'c'];
var arr2 = ['d', 'e', 'f'];

var arr3 = arr1.concat(arr2);
var arr4 = [].concat(arr1, arr2);
console.log(arr3);
console.log(arr4);

console.log('-------------Block Scoped - let---------------');

var a = 12;
var b = 20;

if (a < b) {
    var tmp = a;
    // let tmp = a;
    a = b;
    b = tmp;
}

console.log("a: " + a);
console.log("b: " + b);
console.log("tmp: " + tmp);

console.log('-------------Arrow function---------------');

var hello = function hello(name, messages) {
    console.log('Name: ' + name + ', Message: ' + messages);
};

hello('Uyển', 'Ahihi');

// 1 variable

var hello = function hello(name) {
    console.log('Name: ' + name);
};

hello('Uyển');

// none variable

var hello = function hello() {
    console.log('Name: Uyển');
};

hello();

//this of closure function

var blog = {
    domain: "freetuts.net",

    showWebsite: function showWebsite(callbackFunction) {
        callbackFunction();
    },

    // render : function(){
    //     this.showWebsite(function (){
    //         console.log(this.domain);
    //     }.bind(this)); // ES5
    // },

    render: function render() {
        var _this = this;

        this.showWebsite(function () {
            console.log(_this.domain);
        }); // ES6
    }

    ////----this = blog
};

blog.render();

console.log('-------------Destructuring Assignments---------------');

// Array
var date = ['10', '03', '2016'];
var d = date[0],
    m = date[1],
    y = date[2];


console.log("Day: " + d);
console.log("Month: " + m);
console.log("Year: " + y);

console.log('-------------Const---------------');
//-------------
var info = {
    name: "Uyển",
    domain: "ahihi.net"
};
console.log(info); //object

//-------------
var domain = 'ahihi.net';
// domain = 'qa.ahihi.net';  // can't change value of const variaboe

//-------------Block Scoped
{
    var _domain = 'ahihi.net';
    console.log(_domain);
}

{
    console.log(domain1);
}

console.log('-------------Rest Parameters---------------'); // Strong but it's not good for maintains

var domainList = function domainList(main, sub) {
    for (var _len = arguments.length, other = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
        other[_key - 2] = arguments[_key];
    }

    console.log("Main: " + main);
    console.log("Sub: " + sub);
    console.log(other);
};

domainList('freetuts.net', 'facebook.com', 'google.com', 'zalo.com', 'iphone.com');
//# sourceMappingURL=main.js.map
