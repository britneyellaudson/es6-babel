console.log('-------------Array---------------');

const arr1 = ['a', 'b', 'c'];
const arr2 = ['d', 'e', 'f'];

const arr3 = arr1.concat(arr2);
const arr4 = [...arr1, ...arr2];
console.log(arr3);
console.log(arr4);

console.log('-------------Block Scoped - let---------------');

var a = 12;
var b = 20;

if (a < b)
{
    var tmp = a;
    // let tmp = a;
    a = b;
    b = tmp;
}

console.log("a: " + a);
console.log("b: " + b);
console.log("tmp: " + tmp);

console.log('-------------Arrow function---------------');

var hello = (name, messages) => {
    console.log('Name: '+ name +', Message: ' + messages);
}

hello('Uyển', 'Ahihi');

// 1 variable

var hello = name => {
    console.log('Name: '+ name);
}

hello('Uyển');

// none variable

var hello = () => {
    console.log('Name: Uyển');
}

hello();

//this of closure function

var blog = {
    domain : "freetuts.net",

    showWebsite : function (callbackFunction){
        callbackFunction();
    },

    // render : function(){
    //     this.showWebsite(function (){
    //         console.log(this.domain);
    //     }.bind(this)); // ES5
    // },

    render : function(){
        this.showWebsite(()=>{
            console.log(this.domain);
        });// ES6
    }

    ////----this = blog
};

blog.render();

console.log('-------------Destructuring Assignments---------------');

// Array
let date = ['10', '03', '2016']
let [d, m, y] = date;

console.log("Day: " + d);
console.log("Month: " + m);
console.log("Year: " + y);

console.log('-------------Const---------------');
//-------------
const info = {
    name : "Uyển",
    domain : "ahihi.net"
};
console.log(info); //object

//-------------
const domain = 'ahihi.net';
// domain = 'qa.ahihi.net';  // can't change value of const variaboe

//-------------Block Scoped
{
    const domain1 = 'ahihi.net';
    console.log(domain1);
}


{
    console.log(domain1);
}

console.log('-------------Rest Parameters---------------'); // Strong but it's not good for maintains

let domainList = (main, sub, ...other) =>
{
    console.log("Main: " + main);
    console.log("Sub: " + sub);
    console.log(other);
}

domainList('freetuts.net', 'facebook.com', 'google.com', 'zalo.com', 'iphone.com');